package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CalcFunctionTest {
	
 private static CalcFunction calcFunction=new CalcFunction();
	
 	@Before
	public void setUp() throws Exception {
		calcFunction.clear();
	}

	@Test
	public void testAdd() {
		calcFunction.add("3/2+4/5");
		System.out.println(calcFunction.getResult());
	}

	@Test
	public void testSubstract() {
		
		calcFunction.substract("6-1");
		System.out.println(calcFunction.getResult());
		
	}

	@Test
	public void testMultiply() {
		calcFunction.multiply("3*2");
		System.out.println(calcFunction.getResult());
	}

	@Test
	public void testDivide() {
	
		calcFunction.divide("8��2");
		System.out.println(calcFunction.getResult());
	}

}
