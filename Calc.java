package Test;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Calc {
    public static void main(String args[]){
    	
//        ArrayList<String> question=new ArrayList<String>();
//        System.out.println("请输入要计算的式子个数:");
//        Scanner sc=new Scanner(System.in);
//        int count=sc.nextInt();
//        System.out.println("请输入你要求解的式子:");
//        
//         for(int i=0;i<count;i++){
//              sc=new Scanner(System.in);
//              question.add(sc.nextLine());
//            }
//        for(int i=0;i<count;i++){
//            System.out.print(question.get(i)+"=");
//            CalcFunction calc = new CalcFunction();;
//            calc.calc(question.get(i));
    	
//        }
    	
    	new CalFrame();
    }
    private static CalcFunction calcFunction=new CalcFunction();
    public static class CalFrame extends JFrame {
        private void init() {
            FlowLayout flc = new FlowLayout(FlowLayout.CENTER);
            this.setSize(new Dimension(300, 200));
            this.setLayout(flc);
            JPanel jp = new JPanel(flc);
            final JTextField num1 = new JTextField();
            num1.setColumns(10);
    
            final JTextField res = new JTextField();
            res.setColumns(5);

            jp.add(num1);
           
            jp.add(new JLabel("="));
            jp.add(res);
            this.add(jp);
            JButton jbt = new JButton("计算");
            jbt.addActionListener(new ActionListener() {
     
                @Override
                public void actionPerformed(ActionEvent arg0) {
            
                   String num=num1.getText().toString();
                    calcFunction.calc(num);
                   res.setText(calcFunction.getResult());
                  
                }
            });
            this.add(jbt);
        }
     
        public CalFrame() {
            init();
            this.setDefaultCloseOperation(EXIT_ON_CLOSE);
            this.setResizable(false);
            this.setLocationRelativeTo(null);
            this.setVisible(true);
        }

}
    }